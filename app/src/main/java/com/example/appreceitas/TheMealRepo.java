package com.example.appreceitas;

import com.example.appreceitas.models.Meals;

import java.io.IOException;

public class TheMealRepo {
    private TheMealApi theMealApi;
    public TheMealRepo(TheMealApi api) {
        this.theMealApi = api;
    }

    public Meals getRandomRecipe() throws IOException{
        return theMealApi.fetchRandom().execute().body();
    }
}
