package com.example.appreceitas;

import com.example.appreceitas.models.Meals;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TheMealApi {
    @GET("random.php")
    Call<Meals> fetchRandom();
}
