package com.example.appreceitas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appreceitas.models.Meals;
import com.example.appreceitas.models.Recipe;
import com.example.appreceitas.models.RecipeFacade;
import com.squareup.picasso.Picasso;

import java.util.Map;

public class VerReceitaActivity extends AppCompatActivity {

    private TextView tvMeal;
    private TextView tvFrom;
    private TextView tvTags;
    private TextView tvIngredients;
    private TextView tvInstructions;
    private TextView tvCategory;
    private ImageView thumbMeal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_receita);

        sincronizarComponentes();
        Meals meals = (Meals) getIntent().getSerializableExtra("meals");
        if (meals!= null){
            Recipe recipe= meals.meals.get(0);
            RecipeFacade recipeFac = new RecipeFacade(recipe);
            tvMeal.setText(recipeFac.meal);
            tvFrom.setText("From: "+recipeFac.from);
            tvTags.setText(recipeFac.tags);
            tvCategory.setText("Category: "+recipeFac.category);
            tvInstructions.setText(recipeFac.instructions);
            setIngredients(recipeFac.ingredientsAndMeasure);
            setImage(recipeFac.thumbSrc);
        }
    }

    private void setImage(String thumbSrc) {
        Picasso.get().load(thumbSrc).into(thumbMeal);
    }

    private void setIngredients(Map<String, String> ingredientsAndMeasures) {
        StringBuilder ingredients = new StringBuilder();
        ingredientsAndMeasures.forEach((s, s2) -> {
            ingredients.append(s+"    -----    "+s2+"\n");
        });
        tvIngredients.setText(ingredients);

    }

    private void sincronizarComponentes() {
        this.tvMeal = findViewById(R.id.meal);
        this.tvFrom = findViewById(R.id.from);
        this.tvTags = findViewById(R.id.tags);
        this.tvIngredients = findViewById(R.id.ingredients);
        this.tvInstructions = findViewById(R.id.instructions);
        this.tvCategory = findViewById(R.id.categoria);
        this.thumbMeal = findViewById(R.id.thumb_recipe);
    }
}