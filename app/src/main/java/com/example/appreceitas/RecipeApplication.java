package com.example.appreceitas;

import android.app.Application;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecipeApplication extends Application {

    public Executor executor = Executors.newFixedThreadPool(1);

    private TheMealRepo theMealRepo;

    public TheMealRepo getTheMealRepo() {
        if (theMealRepo != null) return theMealRepo;

        TheMealApi api = new Retrofit.Builder()
                .baseUrl("https://www.themealdb.com/api/json/v1/1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TheMealApi.class);

        theMealRepo = new TheMealRepo(api);
        return theMealRepo;
    }
}
