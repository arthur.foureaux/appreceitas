package com.example.appreceitas.models;


import java.util.LinkedHashMap;

public class RecipeFacade {
    public String meal;
    public String thumbSrc;
    public String category;
    public String from;
    public String instructions;
    public String tags;
    public LinkedHashMap<String, String> ingredientsAndMeasure;

    public RecipeFacade(Recipe recipe){
        ingredientsAndMeasure = new LinkedHashMap<>();
        getInfoFromRecipe(recipe);
        getIngredientsAndMeasures(recipe);
    }

    private void getInfoFromRecipe(Recipe recipe) {
        this.meal = recipe.strMeal;
        this.thumbSrc = recipe.strMealThumb;
        this.category = recipe.strCategory;
        this.from = recipe.strArea;
        this.instructions = recipe.strInstructions;
        this.tags = recipe.strTags;
    }

    private void getIngredientsAndMeasures(Recipe recipe) {

        if (recipe.strIngredient1.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient1, recipe.strMeasure1);
        }
        if (recipe.strIngredient2.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient2, recipe.strMeasure2);
        }
        if (recipe.strIngredient3.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient3, recipe.strMeasure3);
        }
        if (recipe.strIngredient4.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient4, recipe.strMeasure4);
        }
        if (recipe.strIngredient5.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient5, recipe.strMeasure5);
        }
        if (recipe.strIngredient6.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient6, recipe.strMeasure6);
        }
        if (recipe.strIngredient7.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient7, recipe.strMeasure7);
        }
        if (recipe.strIngredient8.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient8, recipe.strMeasure8);
        }
        if (recipe.strIngredient9.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient9, recipe.strMeasure9);
        }
        if (recipe.strIngredient10.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient10, recipe.strMeasure10);
        }
        if (recipe.strIngredient11.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient11, recipe.strMeasure11);
        }
        if (recipe.strIngredient12.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient12, recipe.strMeasure12);
        }
        if (recipe.strIngredient13.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient13, recipe.strMeasure13);
        }if (recipe.strIngredient14.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient14, recipe.strMeasure14);
        }
        if (recipe.strIngredient15.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient15, recipe.strMeasure15);
        }
        if (recipe.strIngredient16.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient16, recipe.strMeasure16);
        }
        if (recipe.strIngredient17.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient17, recipe.strMeasure17);
        }
        if (recipe.strIngredient18.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient18, recipe.strMeasure18);
        }
        if (recipe.strIngredient19.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient19, recipe.strMeasure19);
        }
        if (recipe.strIngredient20.isBlank()){
            return;
        } else {
            ingredientsAndMeasure.put(recipe.strIngredient20, recipe.strMeasure20);
        }



    }

}
